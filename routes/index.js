var express = require("express");
var router = express.Router();

const pageRouter = require("./pages");
const authRouter = require("./auth");
const userRouter = require("./users");
const roomRouter = require("./rooms");
const gameRouter = require("./games");

router.use("/page", pageRouter);
router.use("/auth", authRouter);
router.use("/users", userRouter);
router.use("/rooms", roomRouter);
router.use("/games", gameRouter);

module.exports = router;
