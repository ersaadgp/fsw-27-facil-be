const router = require("express").Router();
const page = require("../controllers/pageController");

const restrict = (req, res, next) => {
  if (req.isAuthenticated()) return next();

  res.redirect("/page/login");
};

router.get("/", restrict, page.home);
router.get("/about", page.about);
router.get("/articles", page.articles);
router.get("/login", page.login);

module.exports = router;
