const router = require("express").Router();
const room = require("../controllers/roomController");
const restrict = require("../middleware/restrict");

router.post("/", room.create);
router.get("/", room.get);

module.exports = router;
