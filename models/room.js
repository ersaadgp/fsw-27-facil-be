"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Room.belongsTo(models.User, {
        foreignKey: "main_player",
        as: "user",
      });
    }
  }
  Room.init(
    {
      name: DataTypes.STRING,
      result: DataTypes.STRING,
      main_player: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Room",
    }
  );
  return Room;
};
