const { Room, User } = require("../models");

exports.get = (req, res) => {
  Room.findAll({ include: ["user"] }).then((result) => {
    res.json({ status: "Fetch Success", result });
  });
};

exports.create = (req, res) => {
  Room.create(req.body).then((result) => {
    res.json({ status: "Create Success", result });
  });
};
