const { Game, Room } = require("../models");

const findWinner = (first, second) => {
  if (first === "rock") {
    if (second === "rock") {
      return "draw";
    }
    if (second === "scissor") {
      return "win";
    }
    if (second === "paper") {
      return "lose";
    }
  }

  if (first === "scissor") {
    if (second === "rock") {
      return "lose";
    }
    if (second === "scissor") {
      return "draw";
    }
    if (second === "paper") {
      return "win";
    }
  }

  if (first === "paper") {
    if (second === "rock") {
      return "win";
    }
    if (second === "scissor") {
      return "lose";
    }
    if (second === "paper") {
      return "draw";
    }
  }
};

const random = () => {
  var result = "";
  var characters = ["rock", "paper", "scissor"];
  var charactersLength = characters.length;
  for (var i = 0; i < 1; i++) {
    result += characters[Math.floor(Math.random() * charactersLength)];
  }
  return result;
};

exports.multiplayer = async (req, res) => {
  const { user_id, room_id, result } = req.body;

  const isNewGame = await Game.findAll({ where: { RoomId: room_id } });
  const isUserExist = await Game.findOne({
    where: { RoomId: room_id, UserId: user_id },
  });

  if (isNewGame.length === 0) {
    Game.create({ UserId: user_id, RoomId: room_id, result })
      .then((respon) => {
        res.status(201).json({ status: "Create Game Success", respon });
      })
      .catch((err) => {
        res.status(400).json({ status: "Create Game Failed", message: err });
      });
  } else if (isNewGame.length === 1) {
    if (isNewGame.length === 1) {
      if (!isUserExist) {
        const gameResult = findWinner(isNewGame[0].result, result);

        Game.create({ UserId: user_id, RoomId: room_id, result })
          .then(() => {
            Room.update(
              { result: gameResult, main_player: isNewGame[0].UserId },
              { where: { id: room_id } }
            )
              .then(() => {
                res.json({ result: `Player One ${gameResult}` });
              })
              .catch((err) => {
                res
                  .status(400)
                  .json({ status: "Create Game Failed", message: err });
              });
          })
          .catch((err) => {
            res
              .status(400)
              .json({ status: "Create Game Failed", message: err });
          });
      } else {
        res.status(400).json({
          status: "Create Game Failed",
          message: "This player is inserted!",
        });
      }
    }
  } else {
    res
      .status(400)
      .json({ status: "Create Game Failed", message: "This room is played!" });
  }
};

exports.singleplayer = async (req, res) => {
  const { user_id, room_id, result } = req.body;

  const isNewGame = await Game.findAll({ where: { RoomId: room_id } });

  const com = random();
  const gameResult = findWinner(result, com);

  if (isNewGame.length === 0) {
    Game.create({ UserId: user_id, RoomId: room_id, result })
      .then(() => {
        Room.update({ result: gameResult }, { where: { id: room_id } })
          .then(() => {
            res.json({ result: `Player One ${gameResult}` });
          })
          .catch((err) => {
            res
              .status(400)
              .json({ status: "Create Game Failed", message: err });
          });
      })
      .catch((err) => {
        res.status(400).json({ status: "Create Game Failed", message: err });
      });
  } else {
    res
      .status(400)
      .json({ status: "Create Game Failed", message: "This room is played!" });
  }
};
