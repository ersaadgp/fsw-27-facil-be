const articles = [
  {
    id: 1,
    title: "What is Lorem Ipsum?",
    body: `Lorem Ipsum is simply dummy text of the printing and typesetting industry.`,
    approved: true,
  },
  {
    id: 2,
    title: "Why do we use it?",
    body: `It is a long established fact.`,
    approved: true,
  },
  {
    id: 3,
    title: "Hello World",
    body: "Cuma lorem ipsum aja kok",
    approved: false,
  },
];

exports.home = (req, res) => {
  const title = "Beranda";
  const subtitle = "Welcome to the world!";

  res.render("index", { title, subtitle });
};

exports.about = (req, res) => {
  const title = "About Page";

  res.render("about", { title });
};

exports.articles = (req, res) => {
  const title = "Article Page";

  res.render("articles", { title, articles });
};

exports.login = (req, res) => {
  const title = "Login Page";
  const messages = { error: null };

  res.render("login", { title, messages });
};
