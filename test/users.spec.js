const request = require("supertest");
const app = require("../app");

describe("Users API", () => {
  test("Get /users --> array users", () => {
    return request(app)
      .get("/users")
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            data: expect.arrayContaining([
              expect.objectContaining({
                username: expect.any(String),
                email: expect.any(String),
                password: expect.any(String),
              }),
            ]),
          })
        );
      });
  });

  test("Get /users/one --> object users", () => {
    return request(app).get("/users/one?username=admin").expect(200);
  });

  test("Get /users/one --> 404", () => {
    return request(app).get("/users/one?username=asd").expect(404);
  });

  //   test("Delete /users/one --> delete one user", async () => {
  //     return request(app).delete("/users?username=admin").expect(200);
  //   });
});
