const request = require("supertest");
const app = require("../app");

describe("Auth API", () => {
  test("Post /auth/login --> login user", async () => {
    return request(app)
      .post("/auth/login")
      .send({
        username: "admin",
        password: "admin",
      })
      .expect(200);
  });
});
